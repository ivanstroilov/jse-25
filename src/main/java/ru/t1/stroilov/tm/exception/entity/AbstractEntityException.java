package ru.t1.stroilov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.exception.AbstractException;

public class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
    }

    public AbstractEntityException(@NotNull String message) {
        super(message);
    }

    public AbstractEntityException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(@NotNull String message, @NotNull Throwable cause, @NotNull boolean enableSuppression, @NotNull boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
