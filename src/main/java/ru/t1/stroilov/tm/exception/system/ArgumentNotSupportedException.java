package ru.t1.stroilov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String message) {
        super("Error! Argument \"" + message + "\" not supported...");
    }
}
