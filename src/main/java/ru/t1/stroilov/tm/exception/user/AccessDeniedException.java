package ru.t1.stroilov.tm.exception.user;

import ru.t1.stroilov.tm.exception.system.AbstractSystemException;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Wrong Login or Password...");
    }

}
