package ru.t1.stroilov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String string = SCANNER.nextLine();
        try {
            return Integer.parseInt(string);
        } catch (final RuntimeException e) {
            throw new NumberIncorrectException(string, e);
        }
    }

}
